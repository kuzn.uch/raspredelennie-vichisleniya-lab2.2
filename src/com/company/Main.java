package com.company;

public class Main {

    public static void main(String[] args) {
        Animal Dog = new Dog("Джесси", " Мясо", " Дача");
        Animal Horse = new Horse("Ева", " Овес", " Ферма");
        Animal Cat = new Cat("Симба", " Корм", " Дом");
        Dog.makeNoise("Джесси");
        Horse.makeNoise("Ева");
        Cat.makeNoise("Симба");
        Dog.eat("Джесси");
        Horse.eat("Ева");
        Cat.eat("Симба");
        Dog.sleep("Джесси");
        Horse.sleep("Ева");
        Cat.sleep("Симба");


        Veterinar veterinar = new Veterinar();
        Animal[] animal = new Animal[3];
        animal[0] = Dog;
        animal[1] = Horse;
        animal[2] = Cat;
        for (Animal a : animal) {
            Veterinar.treatAnimal(a);
        }

    }

    public static class Animal {
        String food;
        String location;
        String name;

        Animal(String name, String food, String location) {
            this.name = name;
            this.food = food;
            this.location = location;
        }

        void makeNoise(String name) {
        }

        void eat(String name) {
            System.out.println(name + " кушает");
        }

        ;

        void sleep(String name) {

            System.out.println(name + " спит");
        }

        ;
    }

    public static class Dog extends Animal {
        public void makeNoise(String name) {
            System.out.println(name + " лает");
        }

        Dog(String name, String food, String location) {
            super(name, food, location);
        }
    }

    public static class Cat extends Animal {
        public void makeNoise(String name) {
            System.out.println(name + " мяукает");
        }

        Cat(String name, String food, String location) {
            super(name, food, location);
        }
    }

    public static class Horse extends Animal {
        public void makeNoise(String name) {
            System.out.println(name + " ржет");
        }

        Horse(String name, String food, String location) {
            super(name, food, location);
        }
    }

    public static class Veterinar {
        public static void treatAnimal(Animal animal) {
            System.out.println(animal.name + " Food:" + animal.food + " Location:" + animal.location);
        }
    }
}
